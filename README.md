# CD Artur Nogueira Projeto Interdisciplinar 2 Semestre 2020

## Ingresso à Reunião de Treinamento do Microsoft Planner no dia 21/10/2020

Para acessar a reunião de hoje garantindo que todos os participantes possam participar ativamente do Chat, cada participante deverá realizar sua inclusão no grupo destinado ao treinamento de hoje e a execução do `Mini Hackathin` na próxima semana.

### Siga os passos abaixo
1. Acesse o Teams e entre o grupo **Equipes** ou **Teams**;
2. Clique no botão `Criar uma equipe ou ingressar nela`, destacado em amarelo conforme a imagem abaixo.
![IMG 01](images/pi_01.PNG).
3. Na próxima tela, para ingressar na equipe, insira o código **jpgkca7** na caixa de texto `Inserir código` conforme a imagem abaixo.
![IMG 02](images/pi_02.PNG)

# Código de acesso: jpgkca7

